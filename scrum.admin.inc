<?php

function scrum_project_overview() {
  $rows = array();
  $header = array(t('Project name'), t('Description'), t('Iteration'), t('Status'), t('Updated'), t('Operations'));

  $result = db_select('scrum', 'sc')
    ->fields('sc', array('project', 'description', 'iteration', 'status', 'updated', 'created'))
    ->orderBy('created', 'desc')
    ->execute();

  foreach ($result as $record) {

    $rows[] = array(
      $record->project,
      $record->description,
      $record->iteration . t(' weeks'),
      $record->status,
      $record->updated,
      l(t('edit'), "DELETE_URL_TO_IMPLEMENT"),
      l(t('delete'), "DELETE_URL_TO_IMPLEMENT"),
    );
  }

  $build['scrum_project_overview_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No project has yet been created.'),
  );

  return $build;
}
