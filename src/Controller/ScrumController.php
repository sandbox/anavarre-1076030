<?php

/**
 * @file
 * Contains \Drupal\scrum\Controller\ScrumController.
 */

namespace Drupal\scrum\Controller;

/**
 * Controller routines for Scrum page routes.
 */
class ScrumController {
  public function Welcome() {
    return array(
      '#markup' => t("<p>Congratulations, you've made it here!</p>"),
    );
  }
}

/**
 * Presents the Scrum project creation form.
 *
 * @return array
 *   A form array as expected by drupal_render().
 */
public function projectAdd() {
  $entity_manager = $this->entityManager();
  $task = $entity_manager->getStorageController('scrum_project')
    ->create(array(
      'refresh' => 3600,
    ));
  return $entity_manager->getForm($task);
}
