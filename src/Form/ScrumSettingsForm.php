<?php

/**
 * @file
 * Contains \Drupal\scrum\Form\ScrumConfigForm.
 */

namespace Drupal\scrum\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for configuring the default Scrum settings.
 */
class ScrumSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'scrum_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'scrum.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('scrum.settings');
    
    $form['iteration'] = array(
      '#title' => $this->t('Sprint iteration'),
      '#description' => t('Set your preferred sprint iteration length. This will be the default for each new project though you can still change this on a per project basis.'),
      '#type' => 'select',
      '#options' => [
         1 => t('1 week'),
         2 => t('2 weeks'),
         3 => t('3 weeks'),
         4 => t('4 weeks'),
         5 => t('5 weeks'),
         6 => t('6 weeks'),
      ],
      '#default_value' => $config->get('sprint_iteration'),
      '#required' => TRUE,
    );

    return parent::buildForm($form, $form_state);
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Implement Validation API https://drupal.org/node/2015613
    $this->config('scrum.settings')

      ->set('sprint_iteration', $form_state->getValue('iteration'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
